import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {NoopInterceptorService} from './noop-interceptor.service';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { SocioComponent } from './socio/socio.component';
import { SalvaEditarComponent } from './socio/salva-editar/salva-editar.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { FooterComponent } from './layout/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { WorkspaceComponent } from './layout/workspace/workspace.component';
import { NavigationComponent } from './layout/navigation/navigation.component';
import { SalvaEditarEmpresaComponent } from './empresa/salva-editar-empresa/salva-editar-empresa.component';
import { ViculoComponent } from './viculo/viculo.component';
import { SocioService } from './socio/socio.service';
import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [
    AppComponent,
    SocioComponent,
    SalvaEditarComponent,
    EmpresaComponent,
    FooterComponent,
    LayoutComponent,
    WorkspaceComponent,
    NavigationComponent,
    SalvaEditarEmpresaComponent,
    ViculoComponent,
    BreadcrumbComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    AngularFontAwesomeModule,
    NgxDatatableModule
  ],
  providers: [
    SocioService, {
    provide: HTTP_INTERCEPTORS,
    useClass: NoopInterceptorService,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
